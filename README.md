# HTTP network load time testing tool #

This tool tests the network load time of pages. Network load time is time spent on sending requests 
and receiving responses. 

## Prerequisites
In order to use the tool you need to have Chrome driver and compatible Chrome or Chromium version installed.

* You can download the latest Chrome driver under https://chromedriver.chromium.org/.
* You can download the latest Chromium browser under https://www.chromium.org/getting-involved/download-chromium.
* You can download the latest Chrome browser under https://www.google.com/chrome/.

## Purpose
This tool was created in order to test page load network time when loading a web page using HTTP/1.1 and HTTP/2.
It uses the Chrome driver and the Chrome DevTools protocol to clear any caches after each page load.

### How do I compile the tool? ###

In order to compile the tool you need to have Rust and Cargo installed. For details how to install Rust for
your platform please visit https://www.rust-lang.org/tools/install.

In order to build the package run:
```
cargo build
```

If you want an optimized build you can use ``cargo build --release``.

The resulting binary will be put in either target/debug or target/release, depending on the type of build.

### How do I use the tool? ###

In order to test a website you first need to start a Chromedriver running on a port of your choosing.
For example:
```chromedriver --port=4444```

Once the driver is running, you can start the tool. You will need to specify the following:

* csv - CSV config file, a sample is provided under sample_cfg.csv. 
  It contains 3 fields, the website to test, number of times to repeat the test, output csv where to store the results.
* chrome - Chrome driver url e.g. http://localhost:4444.

In order to compile and run the binary from the main directory for example:
```
cargo run --bin http-load-time -- --csv ~/sample_cfg.csv --chrome http://localhost:4444
```

You can also find the binary in the target directory after running ``cargo build`` and run it directly:
```
target/http-load-time --csv ~/sample_cfg.csv --chrome http://localhost:4444
```

### Results ###
The results are the combined time spent on loading the resources on the page.