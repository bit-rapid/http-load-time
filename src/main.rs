/*
MIT License
-----------

Copyright (c) 2021 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

use std::fs::OpenOptions;
use std::io::{Write};
use std::path::PathBuf;
use std::thread::sleep;
use std::time::Duration;

use clap::Clap;
use csv;
use serde::Deserialize;
use serde_json::json;
use thirtyfour_sync::{Capabilities, TimeoutConfiguration};
use thirtyfour_sync::extensions::chrome::ChromeDevTools;
use thirtyfour_sync::prelude::*;

#[derive(Debug)]
#[derive(Deserialize)]
struct Record {
    website: String,
    repeats: u32,
    csv_file: String,
}

/// HTTP load time test for Chrome driver test software.
#[derive(Clap)]
#[clap(version = "1.0", author = "F. Minic <filip.minic@bit-rapid.com>")]
struct Opts {
    #[clap(long)]
    csv: String,
    #[clap(long)]
    chrome: String,
}

fn run_iterations(driver: &WebDriver, website: String, repetition: u32, csv_file: String) {
    println!("Starting test on {}", website);
    driver.get(website).unwrap();

    sleep(Duration::new(1, 0));

    let dev_tools = ChromeDevTools::new(&driver.session);

    dev_tools.execute_cdp_with_params("Network.setCacheDisabled",
                                      json!({"cacheDisabled": true})).unwrap();
    let file_path = PathBuf::from(csv_file);

    match std::fs::remove_file(file_path.as_path()) {
        Ok(_) => {}
        Err(_) => {}
    }

    let mut file = OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open(file_path.as_path()).unwrap();

    file.write("Network load time\n".as_bytes()).expect("Could not write to CSV file.");

    for _ in 0..repetition {
        dev_tools.execute_cdp("Network.clearBrowserCache").unwrap();

        driver.refresh().unwrap();
        sleep(Duration::new(0, 100000000u32));

        let mut result = driver.execute_script("return document.readyState;").unwrap();

        while result.value() != "complete" {
            sleep(Duration::new(0, 100000000u32));
            result = driver.execute_script("return document.readyState").unwrap();
        }

        let load_time_script = include_str!("js_scripts/loadtime.js");

        let network_time = driver.execute_script(load_time_script).unwrap();

        let str = format!("{}\n", network_time.value().as_f64().unwrap().to_string().as_str());
        file.write(str.as_bytes()).expect("Could not write to CSV file.");

        println!("Network time: {}", network_time.value().as_f64().unwrap());
    }
}

fn main() -> WebDriverResult<()> {
    let opts: Opts = Opts::parse();

    let mut caps = DesiredCapabilities::chrome();
    caps.accept_ssl_certs(true).unwrap();
    caps.set_application_cache_enabled(false).unwrap();

    let mut driver =
        WebDriver::new(opts.chrome.as_str(), &caps)?;

    driver.set_request_timeout(Duration::new(1200, 0)).unwrap();
    driver.set_timeouts(TimeoutConfiguration::new(Option::Some(Duration::new(600, 0)),
                                                  Option::Some(Duration::new(600, 0)),
                                                  Option::Some(Duration::new(600, 0))))
        .unwrap();

    let file_path = PathBuf::from(opts.csv);
    let mut reader = csv::Reader::from_path(file_path.as_path()).unwrap();

    for result in reader.deserialize::<Record>() {
        let record = result.unwrap();
        run_iterations(&driver, record.website, record.repeats, record.csv_file);
    }

    Ok(())
}
