let entries = performance.getEntriesByType("resource").filter(entry => {
    if (entry.initiatorType !== "other") {
        return entry.transferSize > 0;
    } else {
        return false;
    }
});

let loadDuration = 0;

for (const entry of entries) {
    loadDuration += entry.responseEnd - entry.requestStart;
}

return loadDuration;
