let entry = performance.getEntriesByType("resource").filter(entry => {
    if (entry.initiatorType !== "other") {
        return entry.transferSize > 0;
    } else {
        return false;
    }
})[argument];

return entry;
