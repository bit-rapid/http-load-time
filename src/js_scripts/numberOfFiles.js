function getNumber() {
    let entries = performance.getEntriesByType("resource").filter(entry => {
        if (entry.initiatorType !== "other") {
            return entry.transferSize > 0;
        } else {
            return false;
        }
    });

    let size = 0;

    for (const entry of entries) {
        size++
    }

    return size;
}

return getNumber();
